# Rabobank Assignment #

This repo contains my submission for the Rabobank intake assignment

### How do I get set up? ###

* Open index.html in the browser
* Using the file opener, select a CSV or PRN file from your machine
* The data from that file is now displayed in a html table.
* Disclaimer: This was created under timed conditions for a specific set of files. With no time limit I would ensure all files can be handled and offer up an option for the user to provide a Url to a file instead of having it on their local machine