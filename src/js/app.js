(function() { 
	angular.module('main', []).controller('mainController', ['$scope', '$http', function($scope, $http) {
		$scope.data = {
				titles : [],
				values : []
		};		
		$scope.csvFile = false;

		// Ensures the user's compatability with the FileReader commands is sufficient
		// If it is not, uploading of files from local storage is disabled
		$(document).ready(function() {
    		if(isAPIAvailable()) {
    			// Change to use sockets if I get chance. (emit/on)
      			$('#file').bind('change', handleFileSelect);
    		}
  		});

		// Checks to see if the user meets the requirements for using the FileReader
		function isAPIAvailable() {
		    // Check for the various File API support.
		    if (window.File && window.FileReader && window.FileList && window.Blob) {
		      // Great success! All the File APIs are supported.
		      return true;
		    } else {
		      // source: File API availability - http://caniuse.com/#feat=fileapi
		      document.writeln('The HTML5 APIs used in this form are only available in the following browsers:<br />');
		      // 6.0 File API & 13.0 <output>
		      document.writeln(' - Google Chrome: 13.0 or later<br />');
		      // 3.6 File API & 6.0 <output>
		      document.writeln(' - Mozilla Firefox: 6.0 or later<br />');
		      // 10.0 File API & 10.0 <output>
		      document.writeln(' - Internet Explorer: Not supported (partial support expected in 10.0)<br />');
		      // ? File API & 5.1 <output>
		      document.writeln(' - Safari: Not supported<br />');
		      // ? File API & 9.2 <output>
		      document.writeln(' - Opera: Not supported');
		      return false;
			}
		}

		function handleFileSelect(evt) {
			// Obtain the first file from the array passed by the file explorer on the web page
    		var file = evt.target.files[0];
    		// read the file contents and store them for display in the table
		    printTableFromCSV(file);			    		    
  		}

  		function printTableFromCSV(file) {
    		// Get the contents of the file selected in the file opener
    		var reader = new FileReader();
    		// Converts the file's contents to text
    		if (file.name.substring(file.name.length - 3) === 'prn' || file.name.substring(file.name.length - 3) === 'PRN') { 
    			$scope.csvFile = false;    			
    		}
    		else {
    			$scope.csvFile = true;	    		
			}
			reader.readAsText(file);

	    	// When we have the contents of the file
	    	reader.onload = function(event){
		      	var csv = event.target.result;		      	
		      	// This function will take input in the format of whatever the file.type is and convert those in to objects		      	
		      	if (!$scope.csvFile) { 
					var data = $.csv.toObjects(csv, { separator: '\t', delimiter: '\t' });
					
					// This comes in as a string with undetermined white space
					var titleStr = Object.getOwnPropertyNames(data[0]);		
					// Remove duplicate whitespace and replace with a single space, accounting for first and last character			
					titleStr[0] = titleStr[0].replace(/\s+/g,' ').trim();					
					// Splits the string into an array based on white space					
					$scope.printTitles = titleStr[0];
										
					for(var j = 0; j < data.length; j++) { 						
						$scope.data.values[j] = data[j];
					}					
				}	else { 				
					// Populate data using the plugins functions
					var data = $.csv.toObjects(csv);
					// Store our titles by getting the property names from the first row
					$scope.data.titles =  Object.getOwnPropertyNames(data[0]);
					// Add each array of objects to the main object	
		      		for(var i = 0; i < data.length; i++) {		
			       		$scope.data.values[i] = data[i];			       
		      		}			      		
				}				
	      		// Calling this ensures all the $scope variables are updated due to the watches placed on them by Angular.
	      		// Without this line the changes to scope would not be applied. 
	      		$scope.$apply()
	    	};	    	
	    	// Handle when something is wrong with the file by alerting the user.
	    	reader.onerror = function(){ alert('Unable to read ' + file.fileName); };
  		}

  		// NOT CURRENTLY IMPLEMENTED
  		// In the case of the user trying to link to a file url instead of uploading directly, this function will grab the data and return it
		/*function getData (path) { 
			// Ajax call to the files
			$http.get(path).
  				success(function(data, status, titles, config) {
			    	return data;
			}).
				error(function(data, status, titles, config) {
			    	console.log(data);
			});
		};*/
	}]);
})();